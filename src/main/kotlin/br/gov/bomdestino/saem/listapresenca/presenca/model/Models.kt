package br.gov.bomdestino.saem.listapresenca.presenca.model

import java.time.LocalDate

data class Aluno(val id: String?, val nome: String, val matricula: String)

data class Presenca(val id: String?, val aluno: Aluno, val data: LocalDate)