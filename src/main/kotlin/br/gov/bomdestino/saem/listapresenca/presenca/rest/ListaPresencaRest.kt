package br.gov.bomdestino.saem.listapresenca.presenca.rest

import br.gov.bomdestino.saem.listapresenca.presenca.model.Presenca
import br.gov.bomdestino.saem.listapresenca.presenca.service.PresencaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/presenca")
class ListaPresencaRest {

    @Autowired
    lateinit var presencaService: PresencaService;

    private fun getListaPresenca(): List<Presenca> {
        return presencaService.getPresencas();
    }

    @GetMapping
    fun getPresencas(): List<Presenca> = getListaPresenca()

    @GetMapping("/aluno/{id}")
    fun getPresencaByAluno(@PathVariable id: String): List<Presenca> {
        return presencaService.getPresencasByAluno(id);
    }
}