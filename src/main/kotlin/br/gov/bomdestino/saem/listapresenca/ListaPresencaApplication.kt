package br.gov.bomdestino.saem.listapresenca

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ListaPresencaApplication

fun main(args: Array<String>) {
	runApplication<ListaPresencaApplication>(*args)
}
