package br.gov.bomdestino.saem.listapresenca.presenca.service

import br.gov.bomdestino.saem.listapresenca.presenca.model.Aluno
import br.gov.bomdestino.saem.listapresenca.presenca.model.Presenca
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.*

val alunos = listOf(
        Aluno(UUID.randomUUID().toString(), "Filipe Damasceno", "1111"),
        Aluno(UUID.randomUUID().toString(), "Deusimar Ferreira", "1122")
)

val presencas = listOf(
        Presenca(UUID.randomUUID().toString(), alunos[0], LocalDate.now()),
        Presenca(UUID.randomUUID().toString(), alunos[1], LocalDate.now()),
)

@Service
class PresencaService {

    fun getPresencas(): List<Presenca> = presencas

    fun getPresencasByAluno(alunoID: String): List<Presenca> {
        return getPresencas().filter { it.aluno.id.equals(alunoID) }
    }
}